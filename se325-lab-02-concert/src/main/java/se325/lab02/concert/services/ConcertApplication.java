package se325.lab02.concert.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/services")
public class ConcertApplication extends Application {

    private Set<Object> singletons = new HashSet<>();
    private Set<Class<?>> _classes = new HashSet<Class<?>>();

    public ConcertApplication()
    {
        singletons.add(new ConcertResource());
        _classes.add(SerializationMessageBodyReaderAndWriter.class);
    }

    @Override
    public Set<Object> getSingletons()
    {
        return singletons;
    }

    @Override
    public Set<Class<?>> getClasses() {
        return _classes;
    }
}
